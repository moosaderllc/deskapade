#include "radimil-framework/NCurses/NcursesApp.hpp"

#include <iostream>
using namespace std;

int main()
{
    cout << "Start" << endl;
    
    NcursesApp app;
    app.RegisterColorPair( 1, COLOR_GREEN, COLOR_YELLOW );

    bool done = false;

    while ( done )
    {
        cout << "LOOP" << endl;
        app.DrawCell( 1, 1, 'X', 1 );

        int input = getch();
        if ( input == 'Q' )
        {
            done = true;
        }

        app.DisplayScreen();
    }
    
    cout << "Return 0" << endl;

    return 0;
}
